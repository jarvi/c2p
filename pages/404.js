import { useEffect } from "react"
import { useRouter } from "next/router"
export default function StatusCode404() {
  const router = useRouter()
  useEffect(() => {
    router.replace("/")
  }, [])
}
