import "../scss/global.scss"

import { useMemo, useState, useEffect } from "react"
import "./success.css"
import { GlobalData } from "../context/GlobalData"

function MyApp({ Component, pageProps }) {
  const [formOneData, setFormOneData] = useState("")
  const [formTwoData, setFormTwoData] = useState("")

  const [formOneStatus, setFormOneStatus] = useState("")
  const [formTwoStatus, setFormTwoStatus] = useState("")

  const [stepOneVisibility, setStepOneVisibility] = useState("")
  const [stepTwoVisibility, setStepTwoVisibility] = useState("")
  const [stepThreeVisibility, setStepThreeVisibility] = useState("")

  const formsData = useMemo(
    (_) => ({
      formOneData,
      setFormOneData,
      formTwoData,
      setFormTwoData,

      formOneStatus,
      setFormOneStatus,
      formTwoStatus,
      setFormTwoStatus,

      stepOneVisibility,
      setStepOneVisibility,
      stepTwoVisibility,
      setStepTwoVisibility,
      stepThreeVisibility,
      setStepThreeVisibility,
    }),
    [
      formOneData,
      formTwoData,
      stepOneVisibility,
      formOneStatus,
      formTwoStatus,
      stepTwoVisibility,
      stepThreeVisibility,
    ]
  )

  return (
    <GlobalData.Provider value={formsData}>
      <Component {...pageProps} />
    </GlobalData.Provider>
  )
}

export default MyApp
