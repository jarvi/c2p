import Layout from "../layout/Layaut"
import MainContainer from "../components/MainContainer/MainContainer"
import {PreloaderEffect} from "../components/preloader/Preloader"

export default function Home() {
  return (
    <>
    
      

      <Layout className="home">
      <MainContainer></MainContainer>
      
    </Layout>
    </>
    
  )
}
