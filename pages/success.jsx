import React, { useEffect, useState } from "react"
import Head from "next/head"
import { useRouter } from "next/router"
import useData from "../hooks/useData"

export default function Success() {
  const router = useRouter()
  const [data, setData] = useState(false)
  
  const banks = {
    "0105": "BANCO MERCANTIL C.A.",
    "0134": "BANESCO BANCO UNIVERSAL",
    "0163": "BANCO DEL TESORO",
    "0115": "BANCO EXTERIOR C.A.",
    "0108": "BANCO PROVINCIAL BBVA",
    "0172": "BANCAMIGA",
    "0102": "BANCO DE VENEZUELA",
    "0191": "BANCO NACIONAL DE CREDITO",
    "0104": "BANCO VENEZOLANO DE CREDITO",
    "0114": "BANCO DEL CARIBE C.A.",
  }
  

  const {
    formOneData,
    formTwoData,
    setFormOneData,
    setFormTwoData,
    setFormOneStatus,
    formOneStatus,
    formTwoStatus,
    setFormTwoStatus,
    successAccess,
    setSuccessAccess
  } = useData()

  const [titleBank, setTitleBank] = useState(banks[formTwoData.bank])

  
  
  const backHome = () => {
    setFormOneData(null)
    //setFormTwoData(null)



    // Redirect

    setFormOneData("")
    setFormTwoData("")
    setFormOneStatus("")
    setFormTwoStatus("")

    window.location.href = "/"

  }

  useEffect(() => {

    // Validar si el usuario ejecutó la recarga
    if(formOneStatus === "" && formTwoStatus === ""){
      backHome()
    }


    const getData = {
      phone: formOneData.code+formOneData.phone,
      amount: formOneData.amount,
      code: formOneData.code,
      bank: formTwoData.bank,
      reference: localStorage.getItem("reference"),
    }

    setData(getData)


  }, [])

  const downloadImage = () => {
    //Definimos el botón para escuchar su click
  
    const $objetivo = document.querySelector("#operation-text") // A qué le tomamos la fotocanvas
    // Nota: no necesitamos contenedor, pues vamos a descargarla
  
    const options = {
      ignoreElements: (elemento) => {
        // Una función que ignora elementos. Regresa true si quieres que
        // el elemento se ignore, y false en caso contrario
        const tipo = elemento.nodeName.toLowerCase()
        //console.log(tipo)
  
        // Si es imagen o encabezado h1, ignorar
        if (elemento.id == "btnShare" || elemento.id == "btnDownload") {
          return true
        }
        // Para todo lo demás, no ignorar
        return false
      },
    }
  
    // Ejecutar Html2Canva
    html2canvas($objetivo, options) // Llamar a html2canvas y pasarle el elemento
      .then((canvas) => {
        // Cuando se resuelva la promesa traerá el canvas
        // Crear un elemento <a>
        let enlace = document.createElement("a")
        enlace.download = "Resultados de operacion by Movilnet"
        // Convertir la imagen a Base64
        enlace.href = canvas.toDataURL()
        // Hacer click en él
        enlace.click()
      })
  }

  return (
    formOneStatus && formTwoStatus ? 
    <>
      <Head>
        <title>..::MOVILNET::..</title>
        <link rel="shortcut icon" href="/icons/favicon.gif" />
        <meta
          name="Movilnet"
          property="description"
          content="..::MOVILNET::.."
        />
        <script type="text/javascript" src="/js/html2canvas.min.js"></script>
      </Head>

      <div>
        <div className="box-logo">
          <img src="/images/logo.svg" />
        </div>

        <div className="box-background" id="data">
          <div className="operation-text" id="operation-text">
            <h2 className="success-title">Operación Exitosa</h2>

            <p>Referencia de la operación: {data?.reference}</p>

            <p>Teléfono: {data.phone} </p>
            <p>Monto: Bs. {data?.amount}</p>
            <p>Forma de pago: Pago móvil C2P</p>
            <p>Banco: {titleBank}</p>

            <div className="box-icons">
              <button
                onClick={downloadImage}
                className="btnDownloadMobile"
                id="btnDownload"
              >
                <img src="/icons/download.svg" />
              </button>

              <button
                onClick={downloadImage}
                className="btnDowloadDesktop"
                id="btnDownload"
              >
                Descargar
              </button>

              {/*<button id="btnShare">
              <img src="/icons/share.svg" />
            </button>*/}
            </div>
          </div>
        </div>

        <div className="box-button">
          <button className="btn-recharge" onClick={backHome}>
            Hacer otra recarga
          </button>
        </div>
        
      </div>
    </>
    :
    ""
  )
}



