import classnames from "classnames"
import Header from "../components/Header/Header"
import Head from "next/head";
import {PreloaderEffect} from "../components/preloader/Preloader"
import gsap from "./gsap";

export default function Layout(comp) {
  const { children, className } = comp

 

  return (
    <div className={classnames("layout", { [className]: className })}>
      
      {/** <Head>
        <meta http-equiv="Content-Security-Policy" content="default-src 'self'" />
      </Head> **/}

      <PreloaderEffect />

      <Header />
      <div className="content">{children}</div>
    </div>
  )
}
