import { useRef, useEffect, useState } from "react"
import Carousel from "react-elastic-carousel"
import Item from "./Item"
import useData from "../../hooks/useData"

export default function Header() {
  const c = useRef(0)
  const [imagesSlider, setImagesSlider] = useState(false)

  const { stepOneVisibility, stepTwoVisibility, stepThreeVisibility } =
    useData()

  const imagesMobile = {
    one: "/images/slides/mobile/slide_01.png",
    two: "/images/slides/mobile/slide_02.png",
    three: "/images/slides/mobile/slide_03.png",
    four: "/images/slides/mobile/slide_04.png",
  }

  const imagesDesktop = {
    one: "/images/slides/desktop/slide_01.png",
    two: "/images/slides/desktop/slide_02.png",
    three: "/images/slides/desktop/slide_03.png",
    four: "/images/slides/desktop/slide_04.png",
  }

  useEffect(() => {
    if (stepOneVisibility) {
      c?.current?.goTo(1)
    }

    if (stepTwoVisibility) {
      c?.current?.goTo(2)
    }

    if (stepThreeVisibility) {
      c?.current?.goTo(3)
    }
  }, [stepOneVisibility, stepTwoVisibility, stepThreeVisibility])

  useEffect(() => {
    if (window.innerWidth >= 700) {
      setImagesSlider(imagesDesktop)
    } else {
      setImagesSlider(imagesMobile)
    }

    const intervalCarousel1 = setInterval(() => {
      if (!stepOneVisibility && !stepTwoVisibility && !stepThreeVisibility) {
        if (c?.current.state.activePage == 0) {
          c?.current?.goTo(1)
        }
        if (c?.current.state.activePage == 1) {
          c?.current?.goTo(0)
        }
      }
    }, 4000)

    // Clear
    return () => clearInterval(intervalCarousel1)
  }, [stepOneVisibility, stepTwoVisibility, stepThreeVisibility])

  return (
    <>
      <header className="header">
        <Carousel
          ref={c}
          enableAutoPlay={false}
          initialActiveIndex={0}
          autoPlaySpeed={4000}
          itemsToShow={1}
          showArrows={false}
          pagination={false}
          itemPadding={[0, 0, 0, 0]}
          transitionMs={1000}
          enableMouseSwipe={false}
          enableSwipe={false}
        >
          <img width={"100%"} src={imagesSlider.one} />
          <img width={"100%"} src={imagesSlider.two} />
          <img width={"100%"} src={imagesSlider.three} />
          <img width={"100%"} src={imagesSlider.four} />
        </Carousel>
      </header>
    </>
  )
}
