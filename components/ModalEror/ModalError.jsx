import {useState} from "react";


export const ModalError = ({status, handleStatus, message}) => {
  return (
    <div className={status ? "modal-error-bg activate" : "modal-error-bg"}>
      <div className="modal-error">
        <div className="exit-box">
          <div>
            <img src="/icons/x.svg" width="16px" alt="" onClick={() => handleStatus()} />
          </div>
        </div>

        <img src="/icons/alert.svg" alt="" />

        <h2>Error:</h2>

        <p>{message}</p>

        <br />

      </div>
    </div>
  )
}

