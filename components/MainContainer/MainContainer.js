import { useState, useEffect } from "react"
import useData from "../../hooks/useData"
import Head from "next/head"
import FormStepOne from "../FormStepOne/FormStepOne"
import FormStepTwo from "../FormStepTwo/FormStepTwo"
import StepThree from "../StepThree/StepThree"

export default function MainContainer() {
  const [classEffctOne, setClassEffctOne] = useState(false)
  const [classEffctTwo, setClassEffctTwo] = useState(false)
  const [classEffctThree, setClassEffctThree] = useState(false)

  const {
    formOneData,
    stepOneVisibility,
    setStepOneVisibility,
    stepTwoVisibility,
    setStepTwoVisibility,
    stepThreeVisibility,
    setStepThreeVisibility,
    formOneStatus,
    formTwoStatus,
    crsf,
    setCrsf,
  } = useData()

  useEffect(() => {
    if (stepOneVisibility && formOneStatus) {
      document.querySelector(".bar1").click()
      document.querySelector(".bar2").click()
    }
  }, [formOneStatus])

  useEffect(() => {
    if (stepTwoVisibility && formTwoStatus) {
      document.querySelector(".bar2").click()
      document.querySelector(".bar3").click()
    }
  }, [formTwoStatus])

  const setEffctOne = () => {
    setStepOneVisibility((x) => {
      if (x == "" || x == false) {
        x = true
      } else {
        x = false
      }

      return x
    })
    setClassEffctOne((x) => {
      if (x == "" || x == false) {
        x = true
      } else {
        x = false
      }

      return x
    })

    setClassEffctTwo(false)
    setClassEffctThree(false)
    setStepTwoVisibility(false)
    setStepThreeVisibility(false)
  }

  const setEffctTwo = () => {
    if (formOneStatus) {
      setStepTwoVisibility((x) => {
        if (x == "" || x == false) {
          x = true
        } else {
          x = false
        }

        return x
      })
      setClassEffctTwo((x) => {
        if (x == "" || x == false) {
          x = true
        } else {
          x = false
        }

        return x
      })

      setClassEffctOne(false)
      setStepOneVisibility(false)
      setStepThreeVisibility(false)
      setClassEffctThree(false)
    }
  }

  const setEffctTree = () => {
    if (formTwoStatus) {
      setStepThreeVisibility((x) => {
        if (x == "" || x == false) {
          x = true
        } else {
          x = false
        }

        return x
      })
      setClassEffctThree((x) => {
        if (x == "" || x == false) {
          x = true
        } else {
          x = false
        }

        return x
      })
      setClassEffctOne(false)
      setClassEffctTwo(false)
      setStepOneVisibility(false)
      setStepTwoVisibility(false)
    }
  }

  return (
    <>
      <Head>
        <title>..::MOVILNET::..</title>
        <link rel="shortcut icon" href="/icons/favicon.gif" />
        <meta
          name="Movilnet"
          property="description"
          content="..::MOVILNET::.."
        />
      </Head>
      <section className="main-container">
        <div className="container">
          <div
            className={
              classEffctOne
                ? " step-one step effect-step-one"
                : "  step-one step  "
            }
          >
            <div
              className={formOneStatus ? "bar-success bar bar1" : "bar bar1"}
              onClick={setEffctOne}
            >
              <span className="step-number">1</span>

              <div className="gruop-detail">
                <span className="step-title">
                  Introduzca su número de teléfono
                </span>

                <div className="detail">
                  {formOneStatus && (
                    <>
                      <span>{`${formOneData.code}${formOneData.phone} `}</span>
                      <span className="path">|</span>
                      <span>{` Monto: ${formOneData.amount}`}</span>
                    </>
                  )}
                </div>
              </div>
            </div>
            {stepOneVisibility && <FormStepOne />}
          </div>
          <div
            className={
              classEffctTwo && formOneStatus
                ? "effect-step-two"
                : "step-one step"
            }
          >
            <div
              className={formTwoStatus ? "bar-success bar bar2" : "bar bar2"}
              onClick={setEffctTwo}
            >
              <span className="step-number">2</span>
              <span className="step-title">
                Proceso de pago (Pago móvil C2P)
              </span>
            </div>

            {stepTwoVisibility && formOneStatus && <FormStepTwo />}
          </div>
          <div
            className={
              classEffctThree && formTwoStatus
                ? "effect-step-three"
                : "step-one step"
            }
          >
            <div className="bar bar3" onClick={setEffctTree}>
              <span className="step-number">3</span>
              <span className="step-title">Confirmación de recarga</span>
            </div>
            {stepThreeVisibility && formTwoStatus && <StepThree />}
          </div>
        </div>
      </section>
    </>
  )
}
