import { useState, useEffect } from "react"
import { useForm } from "react-hook-form"
import { yupResolver } from "@hookform/resolvers/yup"
import allowNumbers from "../../hooks/AllowNumber"
import * as yup from "yup"
import useData from "../../hooks/useData"
import { useRouter } from "next/router"

const schema = yup
  .object({
    phone: yup
      .string()
      .required("Este campo es requerido")
      .matches(/^[0-9]+$/, "Solo se admiten números")
      .max(7, "El número móvil ingresado debe tener 7 dígitos")
      .min(7, "El número móvil ingresado debe tener 7 dígitos"),
    
  })
  .required()

 

export default function FormStepOne() {
  const {
    formOneData,
    setFormOneData,
    setStepOneVisibility,
    setStepTwoVisibility,
    setFormOneStatus,
    stepOneVisibility,
  } = useData()
  const router = useRouter()
  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm({
    mode: "onChange",
    resolver: yupResolver(schema),
    defaultValues: {
      phone: formOneData.phone,
      amount: formOneData.amount,
      code: formOneData.code,
    },
  })

  const [amount, setAmount] = useState(formOneData.amount)

  useEffect(() => {
    setStepOneVisibility(true)
  }, [])

  useEffect(() => {
    return () => {
      setStepOneVisibility(false)
    }
  }, [])

  const onSubmit = (data) => {
    setFormOneData({...data, amount})

    setFormOneStatus(isValid)

  }

  const handleAmount = (val) => {
    setAmount(val);
    setFormOneData({...formOneData, "amount": val})
  }


  return (
    <form
      className="form-step-one"
      autoComplete="off"
      onSubmit={handleSubmit(onSubmit)}
    >
        <h4 className="text-center text-form-one">Número a recargar:</h4>
      <div className="content-form">


        <div className="gruop-area-code">
          <select className="area-code" {...register("code")}>
            <option value="0416">0416</option>
            <option value="0426">0426</option>
          </select>
          {errors.code ? <p>{errors.code?.message}</p> : null}
        </div>

        <div className="gruop-area-phone">
          <input
            type="number"
            className="phone"
            placeholder="Ingrese el número a recargar"
            {...register("phone")}
            onKeyDown={(e) => allowNumbers(e, 7)}
          />
          {errors.phone ? <p>{errors.phone?.message}</p> : null}

          {/** <input
            className="amount"
            {...register("amount")}
            type="number"
            onKeyDown={(e) => allowNumbers(e, 2)}
          /> **/}

          {errors.amount ? <p>{errors.amount?.message}</p> : ""}
        </div>
      </div>

      <h4 className="text-center">Seleccione el monto a recargar:</h4>

      <div className="amounts-mobile">
        <div className="scrollmenu">
        
        <a onClick={() => handleAmount(5)} className={formOneData.amount === 5 ? "active": ""}>
          Bs. 5
        </a>

        <a onClick={() => handleAmount(10)} className={formOneData.amount === 10 ? "active": ""}>
          Bs. 10
        </a>

       
         <a onClick={() => handleAmount(15)} className={formOneData.amount === 15 ? "active": ""}>
           Bs. 15
          </a>

          <a onClick={() => handleAmount(25)} className={formOneData.amount === 25 ? "active": ""}>
            Bs. 25
          </a>

          <a onClick={() => handleAmount(30)} className={formOneData.amount === 30 ? "active": ""}>
            Bs. 30
          </a>

          <a onClick={() => handleAmount(45)} className={formOneData.amount === 45 ? "active": ""}>
            Bs. 45
          </a>

          <a onClick={() => handleAmount(50)} className={formOneData.amount === 50 ? "active": ""}>
            Bs. 50
          </a>


          <a onClick={() => handleAmount(60)} className={formOneData.amount === 60 ? "active": ""}>
            Bs. 60
          </a>
        </div>
      </div>

      <div className="amounts-desktop">
      <a onClick={() => handleAmount(5)} className={formOneData.amount === 5 ? "active": ""}>
          Bs. 5
        </a>

        <a onClick={() => handleAmount(10)} className={formOneData.amount === 10 ? "active": ""}>
          Bs. 10
        </a>

       
         <a onClick={() => handleAmount(15)} className={formOneData.amount === 15 ? "active": ""}>
           Bs. 15
          </a>

          <a onClick={() => handleAmount(25)} className={formOneData.amount === 25 ? "active": ""}>
            Bs. 25
          </a>

          <a onClick={() => handleAmount(30)} className={formOneData.amount === 30 ? "active": ""}>
            Bs. 30
          </a>

        
          <a onClick={() => handleAmount(45)} className={formOneData.amount === 45 ? "active": ""}>
            Bs. 45
          </a>

          <a onClick={() => handleAmount(50)} className={formOneData.amount === 50 ? "active": ""}>
            Bs. 50
          </a>


          <a onClick={() => handleAmount(60)} className={formOneData.amount === 60 ? "active": ""}>
            Bs. 60
          </a>
      </div>

      {/*<div className="amounts-desktop">
          <div className="grid-btns">
            <div>Bs. 4</div>
          </div>
        </div> */}

      <div className="actions">
        {isValid && amount ? (
          <button className="accept" type="submit">
            Aceptar
          </button>
        ) : null}
      </div>
    </form>
  )
}
