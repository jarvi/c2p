import React from "react"

export default function Processing({ status }) {
  return (
    <>
      {status ? (
        <div className="preloader-process">
          <div className="preloader">
            <div className="dot"></div>
            <div className="dot"></div>
            <div className="dot"></div>
            <div className="dot"></div>

            <p className="preloader-text">Procesando...</p>
          </div>
        </div>
      ) : (
        ""
      )}
    </>
  )
}
