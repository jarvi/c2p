import { useEffect, useState } from "react"

export default function CustomModal({ setHiddenModal, isHidden, b }) {
  const [state, setstate] = useState([])
  const [title, setTitle] = useState("")

  const handleFather = (_) => {
    setHiddenModal((s) => !s)
  }

  const handleChild = (e) => {
    e.target.classList[0] != "close" ? e.stopPropagation() : setHiddenModal(0)
  }

  useEffect(() => {
    switch (b) {
      case "0134":
        setstate([
          "Solicita tu clave dinámica vía SMS al número 2846, con las palabras “clave dinámica” (sin tilde y con espacio entre ambas palabras).",
          "Ingresa tu literal V, E o J seguido de tu número de cédula o RIF.",
          "Llegará a tu dispositivo móvil una clave de 7 dígitos que podrás utilizar para pagar por las próximas 6 horas.",
        ])

        setTitle("BANESCO BANCO UNIVERSAL")

        break
      case "0105":
        setstate([
          "Envía un SMS al número 24024 con las siglas SCP.",
          "Llegará a tu dispositivo móvil una clave que podrás usar por las próximas 6 horas para pagar.",
        ])

        setTitle("BANCO MERCANTIL C.A.")
        break
      case "0108":
        setstate([
          "Ingresa a la aplicación Dinero Rápido.",
          "Dirígete al Menú y cliquea en “quiero.",
          "Selecciona la opción generar clave de compra” y presiona el botón “generar clave.",
          "Ingresa el monto total de la compra y la clave dinámica para validar el pago.",
        ])
        setTitle("BANCO PROVINCIAL BBVA")
        break
      case "0102":
        setstate([
          "Ingresa a la parte de “personas” en la app Punto Ya BDV.",
          "Autentica tus credenciales de usuario único.",
          "Elige en el menú de opciones “configuración”.",
          "Selecciona “claves de pago” y posteriormente “configuración”, cliquea la opción de SMS",
          "Llegará a tu número telefónico afiliado a Clavemóvil un SMS con el monto de la operación y un código de confirmación.",
        ])
        setTitle("BANCO DE VENEZUELA")
        break
      case "0191":
        setstate([
          "Ingresa a la Web (www.bnc.com.ve) del banco.",
          "Dirígete a “personas” y completa los datos requeridos.",
          "Cliquea el menú principal. Luego “pagos” y, finalmente, “pago móvil”.",
          "Presiona la opción “C2P”, selecciona la cantidad de claves a generar y luego a “continuar”.",
          "Introduce el número de control, número de coordenadas de tu BINGO BNC que solicite el sistema y luego “continuar”.",
          "Las claves C2P serán generadas y podrás imprimirlas.",
        ])
        setTitle("BANCO NACIONAL DE CREDITO")
        break
      case "0104":
        setstate([
          "Ingresa a la aplicación BVC Integrador.",
          "Cliquea en el menú de inicio “clave dinámica”.",
          "Comparte la clave dinámica y realiza la transacción.",
        ])
        setTitle("BANCO VENEZOLANO DE CREDITO")
        break
      case "0172":
        setstate([
          "Ingresa a Bancamiga Suit.",
          "Introduce los datos solicitados.",
          "Elige la opción “pago móvil”.",
          "Cliquea y genera la clave C2P.",
          "Compártela con el comercio y realiza tu pago.",
        ])
        setTitle("BANCAMIGA")
        break
      case "0115":
        setstate([
          "Envía un SMS al número 278 con el literal (V, E o P) y número de cédula.",
        ])
        setTitle("BANCO EXTERIOR C.A.")
        break

      case "0163":
        setstate([
          "Envía un SMS al número 2383 con la palabra “comercio”, literal (V o E), número de cédula y una coordenada a elegir.",
        ])
        setTitle("BANCO DEL TESORO")
        break

      case "0114":
        setstate([
          "Envía un SMS al número 22741 con la palabra “ClaveMipago”.",
          "Recibirás un SMS con la clave dinámica.",
        ])
        setTitle("BANCO DEL CARIBE C.A.")
        break

      default:
        setstate(["Selecciona un banco"])
    }
  }, [b])

  return (
    <>
      {!isHidden ? (
        <div onClick={handleFather} className="custom-modal">
          <div onClick={(e) => handleChild(e)} className="dialog">
            <div className="modal-header">
              <span className="close"></span>
            </div>
            <div className="modal-content">
              <span className="modal-title">{title}</span>
              <div className="elements">
                {state.map((item, index) => (
                  <div className="group" key={index}>
                    {/* <span className="iten"></span> */}
                    <li className="item-bank" key={index}>
                      {item}
                    </li>
                  </div>
                ))}
              </div>
            </div>
            <div className="modal-action"></div>
          </div>
        </div>
      ) : (
        ""
      )}
    </>
  )
}
