import React, { useEffect} from "react";


// Preloader Component
export const Preloader = () => {
    useEffect(() => {

    }, [])

    return(
    <div className="preloader">
      <div className="ball">
      </div>

        <div className="box-logo">
            <img src="/logo.png" id="logo" />
        </div>
    </div>
  )};

  //APP Component
 export const PreloaderEffect = () => {
    const [animationComplete, setAnimationComplete] = React.useState(false);

    const completeAnimation = () => {
      setAnimationComplete(true);
    };

    React.useEffect(() => {
      // GSAP animation
      let tl = gsap.timeline();
      const homeAnimation = (animation) => {
        tl.to(".ball", {
          duration: 2,
          y: "100vh",
          ease: "bounce.out"
        })
          .to(".ball", {
            duration: 1.4,
            scale: 30,
            ease: "power3.out",
            onComplete: animation
          })
          
          
      };
      homeAnimation(completeAnimation);
    }, []);
    
    return (
      <>
        {animationComplete === false && <Preloader />}

      </>
    );
  };