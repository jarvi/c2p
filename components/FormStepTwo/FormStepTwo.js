import { useState, useEffect } from "react"
import { useForm } from "react-hook-form"

import { yupResolver } from "@hookform/resolvers/yup"
import allowNumbers from "../../hooks/AllowNumber"
import * as yup from "yup"
import useData from "../../hooks/useData"
import CustomModal from "../CustomModal/CustomModal"

const schema = yup
  .object({
    phone_d: yup
      .string("debe ser un numero")
      .required("Este campo es requerido")
      .max(7, "El número móvil ingresado debe tener 7 dígitos")
      .min(7, "El número móvil ingresado debe tener 7 dígitos"),
    input_dni: yup
      .string()
      .required("Este campo es requerido")
      .matches(/^[0-9]+$/, "Solo se admiten números")
      .min(6, "El número ingresado debe tener minimo 6 dígitos")
      .max(9, "El número ingresado debe tener maximo 9 dígitos"),
    select_dni: yup.string().required("Este campo es requerido"),
    bank: yup.string().required("Este campo es requerido"),
    input_token: yup
      .string()
      .required("Este campo es requerido")
      .max(8, "Este1 campo  debe tener maximo 8 caracteres"),
  })
  .required()

export default function FormStepTwo() {
  const {
    setFormTwoData,
    setStepTwoVisibility,
    setFormTwoStatus,
    formTwoData,
  } = useData()

  const [isHidden, setHiddenModal] = useState(true)

  const {
    register,
    handleSubmit,
    setValue,
    getValues,
    formState: { errors, isValid },
  } = useForm({
    mode: "onChange",
    defaultValues: {
      input_token: formTwoData,
      phone_d: formTwoData.phone_d,
      input_dni: formTwoData.input_dni,
      select_dni: formTwoData.select_dni,
      bank: formTwoData.bank,
      input_token: formTwoData.input_token,
      code_area: formTwoData.code_area,
    },

    resolver: yupResolver(schema),
  })

  const [selectedBank, setSelectedBank] = useState(getValues("bank"))
  const onSubmit = (data) => {
    setFormTwoData(data)
    setFormTwoStatus(isValid)
  }

  useEffect(() => {
    setStepTwoVisibility(true)
  }, [])

  useEffect(() => {
    return () => {
      setStepTwoVisibility(false)
    }
  }, [])

  const f = () => {
    setHiddenModal(false)
  }

  return (
    <form
      className="form-step-two"
      autoComplete="off"
      onSubmit={handleSubmit(onSubmit)}
    >

      <div className="content-form">
        <h4 className="text-center title-form-two-first">Número a recargar:</h4>
        
        <div className="group-dni">
          <select className="select-dni" {...register("code_area")}>
            <option value="0416">0416</option>
            <option value="0426">0426</option>
            <option value="0414">0414</option>
            <option value="0424">0424</option>
            <option value="0424">0412</option>
          </select>
          <div className="group-dni-d">
            <input
              className="input-dni"
              type="number"
              placeholder={"Ingrese el numero de teléfono"}
              onKeyDown={(e) => allowNumbers(e, 7)}
              {...register("phone_d")}
            />
            {errors.phone_d?.message && <p>{errors.phone_d?.message}</p>}
          </div>

          <div className="group-icon">
            <span className="icon">?</span>
          </div>
        </div>

        <h4 className="text-center title-form-two">
          Seleccione el banco de C2P:
        </h4>

        <div className="group-select-banks">


          <select
            className="select-banks"
            defaultValue=""
            {...register("bank", {
              onChange: (_) => {
                setSelectedBank(getValues("bank"))
              },
            })}
          >
            <option className="disable" value="" disabled>
              Banco
            </option>
            <option value="0105">BANCO MERCANTIL C.A.</option>
            <option value="0134">BANESCO BANCO UNIVERSAL</option>
            <option value="0163">BANCO DEL TESORO</option>
            <option value="0115">BANCO EXTERIOR C.A.</option>
            <option value="0108">BANCO PROVINCIAL BBVA</option>
            <option value="0172">BANCAMIGA</option>
            <option value="0102">BANCO DE VENEZUELA</option>
            <option value="0191">BANCO NACIONAL DE CREDITO</option>
            <option value="0104">BANCO VENEZOLANO DE CREDITO</option>
            <option value="0114">BANCO DEL CARIBE C.A.</option>
          </select>
        </div>

        <h4 className="text-center title-form-two">
          Número de cédula:
        </h4>

        <div className="group-dni">
          <select className="select-dni" {...register("select_dni")}>
            <option value="V">V</option>
            <option value="E">E</option>
          </select>

          <div className="group-dni-d">
            <input
              className="input-dni"
              type="number"
              placeholder={"Ingrese el numero de cédula"}
              onKeyDown={(e) => allowNumbers(e, 8)}
              {...register("input_dni")}
            />
            {errors.input_dni?.message && <p>{errors.input_dni?.message}</p>}
          </div>

          <div className="group-icon">
            <span className="icon">?</span>
          </div>
        </div>

        <div className="gruop-token">
          <span className="token">Token:</span>

          <div className="group-t-h">
            <div className="group-icon-input">
              <input
                type="text"
                maxLength={8}
                placeholder={"Ingrese el token de su banco"}
                className="input-token"
                {...register("input_token", {
                  onChange: () => {
                    let pattern2 = /^[A-Za-z0-9]{0,8}$/g
                    if (pattern2.test(getValues("input_token"))) {
                      setValue("input_token", getValues("input_token"))
                    } else {
                      let t = getValues("input_token").substring(
                        0,
                        getValues("input_token").length - 1
                      )
                      setValue("input_token", t)
                    }
                  },
                })}
              />

              {errors.input_token ? <p>{errors.input_token?.message}</p> : ""}
            </div>

            <div onClick={(_) => f()} className="group-icon help">
              <span className="icon">?</span>
            </div>
          </div>
        </div>

        <div className="actions">
          <CustomModal
            setHiddenModal={setHiddenModal}
            isHidden={isHidden}
            b={selectedBank}
            key={Math.random()}
          ></CustomModal>
          {isValid ? (
            <button className="accept" type="submit">
              Confirmar Pago
            </button>
          ) : null}
        </div>
      </div>
    </form>
  )
}
