import { useEffect, useState } from "react"
import { useRouter } from "next/router"
import useData from "../../hooks/useData"
import axios from "axios"
import { ModalError } from "../ModalEror/ModalError"
import { CURRENT_URI } from "../../utils/contants"
import Processing from "../processing/Processing"
// Get User Information

export default function StepThree() {
  const router = useRouter()
  const { formOneData, formTwoData, setStepThreeVisibility, crsf } = useData()

  const [statusModal, setStatusModal] = useState(false)
  const [userInformation, setUserInformation] = useState(false)
  const [messageError, setMessageError] = useState(false)

  const handleStatusModal = () => setStatusModal(!statusModal)

  const banks = {
    "0105": "BANCO MERCANTIL C.A.",
    "0134": "BANESCO BANCO UNIVERSAL",
    "0163": "BANCO DEL TESORO",
    "0115": "BANCO EXTERIOR C.A.",
    "0108": "BANCO PROVINCIAL BBVA",
    "0172": "BANCAMIGA",
    "0102": "BANCO DE VENEZUELA",
    "0191": "BANCO NACIONAL DE CREDITO",
    "0104": "BANCO VENEZOLANO DE CREDITO",
    "0114": "BANCO DEL CARIBE C.A.",
  }

  const [titleBank, setTitleBank] = useState(banks[formTwoData.bank])
  const [statusProcessing, setStatusProcessing] = useState(false)

  // URL
  const url = CURRENT_URI + "api/compra"

  // Init component
  useEffect(() => {
    fetch("https://ipinfo.io/json?token=41b85250a6100d")
      .then((response) => response.json())
      .then((jsonResponse) => setUserInformation(jsonResponse))
      .catch((error) => error)
  }, [])

  useEffect(() => {
    setStepThreeVisibility(true)
  }, [])

  useEffect(() => {
    return () => {
      setStepThreeVisibility(false)
    }
  }, [])

  const recargar = () => {
    if(!statusProcessing){
    setStatusProcessing(true);

    // Token CRSF
    //axios.get("http://10.10.20.22:3900/api/form")
    //.then(response => {
    //console.log(response.data.csrfToken)
    // Make a request using the Fetch API
    axios
      .post(url, {
        monto: formOneData.amount,
        numero: `${formOneData.code}${formOneData.phone}`,
        token: formTwoData.input_token,
        banco_origen: formTwoData.bank,
        telefono_cliente: `${formTwoData.code_area}${formTwoData.phone_d}`,
        cedula_cliente: `${formTwoData.select_dni}${formTwoData.input_dni}`,
        longitud: "10.66666",
        latitud: "-65.25641",
        sistema_operativo: "Linux",
        ip: "200.75.135.130",
      })
      .then((response) => {
        localStorage.setItem("reference", response.data.referencia)

        // Redirect
        router.replace("/success")

        setTimeout(() => {
          setStatusProcessing(false);
        }, 1000)
      })
      .catch( (error) => {
        setStatusProcessing(false);
        
        if (error.message === "Network Error") {
           setMessageError("No hay conección")
        } else if (error?.response?.data.mensaje) {
           setMessageError(error?.response?.data?.mensaje)
        } else {
          setMessageError("Intente nuevamente")
        }

        setStatusModal(true)
      })
    }
    //})

    // Data necesaria para crear la peticion al servidor
    // axios.post(url, {
    //   monto: formOneData.amount,
    //   numero: formOneData.code+formOneData.phone,
    //   token: formTwoData.input_token,
    //   sistema_operativo: window.navigator.platform,
    //   cedula_cliente: formTwoData.select_dni + formTwoData.input_dni,
    //   telefono_cliente: formTwoData.code_area + formTwoData.phone_d,
    //   ip: userInformation.ip,
    //   banco_origen: formTwoData.bank,
    //   longitud:"10.66666",
    //   latitud: "-65.25641",
    //   _csrf: localStorage.getItem("crsf")
    // })
    // .then((response) => {
    //   localStorage.setItem('reference', response.data.referencia);

    //   // Redirect
    //   setSuccessAccess(true);
    //   router.replace("/success")

    // })
    // .catch(async error => {

    //   if(error.message === "Network Error"){
    //     await setMessageError("No hay conección")
    //   }else{
    //     await setMessageError(error?.response?.data?.error)
    //   }

    //   setStatusModal(true);

    // });
  }

  useEffect(() => {
    setTitleBank(banks[formTwoData.bank])
  }, [formTwoData.bank])

  const validation = () => {
    if (true) {
      // Call service
      recargar()
    }
  }

  return (
    <>
      <ModalError
        message={messageError}
        status={statusModal}
        handleStatus={handleStatusModal}
      />

      <Processing status={statusProcessing} />

      <div className="step-three">
        {formOneData && formTwoData && (
          <>
            <div className="container-step-three">
              <span>Teléfono: {formOneData.code + formOneData.phone}</span>
              <span>Monto:Bs {formOneData.amount}</span>
              <span>Forma de pago: {"Pago móvil C2P"}</span>
              <span>Banco: {titleBank}</span>
            </div>
            <div className="actions">
              <button className="recharge" onClick={validation}>
                Recargar
              </button>
            </div>
          </>
        )}
      </div>
    </>
  )
}
