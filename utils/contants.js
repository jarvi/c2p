const BASE_PATH = {
  local: "http://10.10.20.22:3900/",
  test: "http://c2p.movilnet.pagospayall.com/",
  prod: "http://c2pmovilnet.payall.com/",
  qa:"https://c2pmovilnetqa.pagospayall.com/"
}

export const CURRENT_URI = BASE_PATH.test
