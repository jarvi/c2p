export default function allowNumbers(e, l) {
  if (
    !(
      (e.keyCode > 95 && e.keyCode < 106) ||
      (e.keyCode > 47 && e.keyCode < 58) ||
      e.keyCode == 8
    ) ||
    (e.target.value.length == l && e.keyCode != 8)
  ) {
    e.preventDefault()
  }
}
