import { useContext } from "react"
import { GlobalData } from "../context/GlobalData"

export default () => useContext(GlobalData)
