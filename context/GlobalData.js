import { createContext } from "react"

export const GlobalData = createContext({
  formOneData: null,
  setFormOneData: () => null,
  formTwoData: null,
  setFormTwoData: () => null,

  amount: null,
  setAmount: () => null,

  formOneStatus: null,
  setFormOneStatus: () => null,
  formTwoStatus: null,
  setFormTwoStatus: () => null,

  stepOneVisibility: null,
  setStepOneVisibility: () => null,
  stepTwoVisibility: null,
  setStepTwoVisibility: () => null,
  stepThreeVisibility: null,
  setStepThreeVisibility: () => null,


})
